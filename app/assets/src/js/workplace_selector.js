//
// OpenGDToolbox : collaborative web app made for game creators to simplify game
// design work flow and collaboration.
// Copyright (C) 2019 Thibaud FAURIE
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

function registerWorkplace(id, button){
  $('#loading-spinner').show();

  $(button).prop('disabled', true);
  $(button).html(
    `Loading...`
  );

  clearAllTabs();

  $.ajax({
        type     : "POST",
        url      : '/ajax/wp',
        data     : {workplace: id},
        cache    : false,
        success  : function()
        {
            unregisterWorkgroup();
            addMessage('Successfuly registered to workplace.', 'success');
            $('#workplaces_list .card').each(function(){
              $(this).find('.card-header').removeClass('bg-success');
              $(this).removeClass('border-success');
              $(this).find('.card-body button').removeClass('btn-success');
              $(this).find('.card-body button').addClass('btn-secondary');
              $(this).find('.card-body button').html('<i class="fas fa-check"></i>');
              $(this).find('.card-body button').prop('disabled', false);
            });

            $(button).parent().prev().addClass('bg-success');
            $(button).parent().parent().addClass('border-success');
            $(button).removeClass('btn-secondary');
            $(button).addClass('btn-success');
            $(button).html(
              `Registered`
            );
            $(button).prop('disabled', true);
            $('#loading-spinner').hide();

            $('#workgroup_dropdown').trigger('content:reload');
            $('#project_dropdown').trigger('content:reload');
        },
        error: function(){

            addMessage('Failed to register in workplace.', 'danger');
            $(button).removeClass('btn-secondary');
            $(button).addClass('btn-alert');
            $(button).html(
              `Fail !`
            );
            $('#loading-spinner').hide();
        },
  });
}
