//
// OpenGDToolbox : collaborative web app made for game creators to simplify game
// design work flow and collaboration.
// Copyright (C) 2019 Thibaud FAURIE
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

$(document).ready(testProject());

$('#project_dropdown').on('content:reload', function(){

    $('#project-spinner').show();
    $.ajax({
        type : 'GET',
        url : '/api/project',
        cache : false,
        data : {'format':'json'},
        success : function(data){
          if (data.length == 0){
            $('#project_dropdown').find('a.dropdown-toggle').addClass('disabled');
            $('#project_button_text').html('No project exists');
          }else{
            $('#project_list').html('');
            $.each(data, function(index, value){
              $('#project_list').append(`<a onclick="registerProject(`+value['id']+`,'`+value['label']+`')"
                  class="dropdown-item" href="#!">
                  <i class="mr-3 text-dark fas fa-gamepad"></i>`+value['label']+`
              </a>`);
            });
            $('#project_dropdown').find('a.dropdown-toggle').removeClass('disabled');
          }
          $('#project-spinner').hide();
        },
        error : function(data){
          addMessage("Failed to retrieve projects !", 'danger');
          $('#project_dropdown').find('a.dropdown-toggle').addClass('disabled');
          $('#project-spinner').hide();
        },
    });
});

function registerProject(id = '', label = ''){
  $('#project-spinner').show();

  $.ajax({
        type     : "POST",
        url      : '/ajax/pj',
        data     : {project: id, project_label: label, change: true},
        cache    : false,
        success  : function(data)
        {
            $('#project_button_text').html(label)
            $('#project-spinner').hide();
            addMessage('Successfuly switched to project '+label+'.', 'success');
            $('#project_dropdown').trigger('content:reload');
            clearAllTabs();
        },
        error: function(){

            addMessage('Failed to switch to project '+label+'.', 'danger');
            $('#project-spinner').hide();
        },
  });
}

function testProject(){
  $('#project-spinner').show();

  $.ajax({
        type     : "POST",
        url      : '/ajax/pj',
        cache    : false,
        success  : function(data)
        {
            if(data['registered'] !== undefined){
                $('#project_button_text').html(data['registered']); // Test if already registered
            }
            $('#project-spinner').hide();
        },
        error: function(){
            addMessage('Failed to fetch project.', 'danger');
            $('#project-spinner').hide();
            clearAllTabs();
        },
  });
}

function unregisterProject(){
  $('#workgroup-spinner').show();

  clearAllTabs();

  $.ajax({
        type     : "POST",
        url      : '/ajax/pj/del',
        cache    : false,
        success  : function(data)
        {
            $('#project_button_text').html('Select a project');
            $('#project-spinner').hide();
        },
        error: function(){
            addMessage('Failed to unregister from current project.', 'danger');
            $('#project-spinner').hide();
        },
  });
}
