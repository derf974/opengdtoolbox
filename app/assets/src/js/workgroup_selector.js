//
// OpenGDToolbox : collaborative web app made for game creators to simplify game
// design work flow and collaboration.
// Copyright (C) 2019 Thibaud FAURIE
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

$(document).ready(testWorkgroup());

$('#workgroup_dropdown').on('content:reload', function(){

    $('#workgroup-spinner').show();
    $.ajax({
        type : 'GET',
        url : '/api/workgroup',
        cache : false,
        data : {'format':'json'},
        success : function(data){
          if (data.length == 0){
            $('#workgroup_dropdown').find('a.dropdown-toggle').addClass('disabled');
            $('#workgroup_button_text').html('No workgroup exists');
          }else{
            $('#workgroup_list').html('');
            $.each(data, function(index, value){
              $('#workgroup_list').append(`<a onclick="registerWorkgroup(`+value['id']+`,'`+value['label']+`')"
                  class="dropdown-item" href="#!">
                  <i class="mr-3 text-dark fas fa-folder-open"></i>`+value['label']+`
              </a>`);
            });
            $('#workgroup_dropdown').find('a.dropdown-toggle').removeClass('disabled');
          }
          $('#workgroup-spinner').hide();
        },
        error : function(data){
          addMessage("Failed to retrieve Workgroups !", 'danger');
          $('#workgroup-spinner').hide();
        },
    });
});

function registerWorkgroup(id = '', label = ''){
  $('#workgroup-spinner').show();

  $.ajax({
        type     : "POST",
        url      : '/ajax/wg',
        data     : {workgroup: id, workgroup_label: label, change: true},
        cache    : false,
        success  : function(data)
        {
            $('#workgroup_button_text').html(label)
            $('#workgroup-spinner').hide();
            addMessage('Successfuly switched to workgroup '+label+'.', 'success');
            $('#project_dropdown').trigger('content:reload');
            clearAllTabs();
        },
        error: function(){

            addMessage('Failed to switch to workgroup '+label+'.', 'danger');
            $('#workgroup-spinner').hide();
        },
  });
}

function testWorkgroup(){
  $('#workgroup-spinner').show();

  $.ajax({
        type     : "POST",
        url      : '/ajax/wg',
        cache    : false,
        success  : function(data)
        {
            if(data['registered'] !== undefined){
                $('#workgroup_button_text').html(data['registered']); // Test if already registered
            }
            $('#workgroup-spinner').hide();
        },
        error: function(){
            addMessage('Failed to fetch workgroup.', 'danger');
            $('#workgroup-spinner').hide();
            clearAllTabs();
        },
  });
}

function unregisterWorkgroup(){
  $('#workgroup-spinner').show();

  clearAllTabs();

  $.ajax({
        type     : "POST",
        url      : '/ajax/wg/del',
        cache    : false,
        success  : function(data)
        {
            unregisterProject();
            $('#workgroup_button_text').html('Select a workgroup');
            $('#workgroup-spinner').hide();
        },
        error: function(){
            addMessage('Failed to unregister from current workgroup.', 'danger');
            $('#workgroup-spinner').hide();
        },
  });
}
