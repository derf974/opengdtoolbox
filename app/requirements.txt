coverage==4.5.3
Django==2.1.7
django-activity-stream==0.7.0
djangorestframework==3.9.2
django-cors-headers==2.5.2
django-filter==2.1.0
psycopg2-binary==2.7.7
pytz==2018.9
