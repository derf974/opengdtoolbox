//
// OpenGDToolbox : collaborative web app made for game creators to simplify game
// design work flow and collaboration.
// Copyright (C) 2019 Thibaud FAURIE
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require("gulp-rename");

gulp.task('all', ['styles', 'js', 'fa', 'icons', 'animate']);

gulp.task('styles', function() {
    gulp.src('assets/src/scss/style.scss')
    .pipe(sass())
    .pipe(gulp.dest('assets/static/css/'))
});

gulp.task('js', function() {
    gulp.src('node_modules/jquery/jquery.min.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('node_modules/jquery/jquery.min.map')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('node_modules/jquery-mousewheel/jquery.mousewheel.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('node_modules/bootstrap/dist/js/bootstrap.min.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('node_modules/bootbox/dist/bootbox.all.min.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('node_modules/popper.js/dist/umd/popper.min.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('node_modules/@fortawesome/fontawesome-free/js/all.min.js')
    .pipe(rename('fontawesome.min.js'))
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('assets/src/js/scripts.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('assets/src/js/tab.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('assets/src/js/holder.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('node_modules/js-cookie/src/js.cookie.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('assets/src/js/project_selector.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('assets/src/js/workgroup_selector.js')
    .pipe(gulp.dest('assets/static/js/'));
    gulp.src('assets/src/js/workplace_selector.js')
    .pipe(gulp.dest('assets/static/js/'));
});

gulp.task('fa', function() {
    gulp.src('node_modules/@fortawesome/fontawesome-free/scss/fontawesome.scss')
    .pipe(sass())
    .pipe(gulp.dest('assets/static/css'))
});

gulp.task('icons', function() {
    gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
    .pipe(gulp.dest('assets/static/webfonts/'));
});

gulp.task('animate', function() {
    gulp.src('node_modules/animate.css/animate.min.css')
    .pipe(gulp.dest('assets/static/css/'));
});
