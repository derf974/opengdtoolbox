#
# OpenGDToolbox : collaborative web app made for game creators to simplify game
# design work flow and collaboration.
# Copyright (C) 2019 Thibaud FAURIE
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

# manager/serializers.py

from rest_framework import serializers
from .models import Profile, Workplace, Workgroup, Project

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            'id',
            'user',
            'bio',
            'firstname',
            'lastname',
            'email',
            'location',
            'time_zone',
            'birth_date',
            'modified_at',
            'created_at',
            )

class WorkplaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workplace
        fields = (
            'id',
            'label',
            'owner_type',
            'users',
            'created_at',
            'modified_at',
            )

class WorkgroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workgroup
        fields = (
            'id',
            'label',
            'users',
            'workplace',
            'created_at',
            'modified_at',
            )

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = (
            'id',
            'label',
            'workgroups',
            'workplace',
            'created_at',
            'modified_at',
            )
