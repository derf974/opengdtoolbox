#
# OpenGDToolbox : collaborative web app made for game creators to simplify game
# design work flow and collaboration.
# Copyright (C) 2019 Thibaud FAURIE
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

from django.db import models
from django.urls import reverse

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Const

OWNER_TYPES = [
    ("company","company"),
    ("organization","organization"),
    ("individual","individual"),
]

TIMEZONES = [
    ("UTC-11","UTC-11"),("UTC-10","UTC-10"),("UTC-9","UTC-9"),("UTC-8","UTC-8"),
    ("UTC-7","UTC-7"),("UTC-6","UTC-6"),("UTC-5","UTC-5"),("UTC-4","UTC-4"),("UTC-3","UTC-3"),
    ("UTC-2","UTC-2"),("UTC-1","UTC-1"),

    ("UTC","UTC"),

    ("UTC+1","UTC+1"),("UTC+2","UTC+2"),("UTC+3","UTC+3"),("UTC+4","UTC+4"),
    ("UTC+5","UTC+5"),("UTC+6","UTC+6"),("UTC+7","UTC+7"),("UTC+8","UTC+8"),("UTC+9","UTC+9"),
    ("UTC+10","UTC+10"),("UTC+11","UTC+11"),("UTC+12","UTC+12"),
]


# Create your models here.

class Profile(models.Model):

    class Meta:
        verbose_name = "profile"
        verbose_name_plural = "profiles"

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    firstname = models.CharField(max_length=150, blank=True)
    lastname = models.CharField(max_length=150, blank=True)
    email = models.EmailField(max_length=254, blank=False)
    location = models.CharField(max_length=150, blank=True)
    time_zone = models.CharField(default="UTC",max_length=50,choices=TIMEZONES)
    birth_date = models.DateField(null=True, blank=True)

    modified_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.__str__()

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

class Workplace(models.Model):

    class Meta:
        verbose_name = "workplace"
        verbose_name_plural = "workplaces"

    label = models.CharField(max_length=150, null=False, blank=False)
    owner_type = models.CharField(max_length=150, choices=OWNER_TYPES, blank=False, null=False)
    users = models.ManyToManyField(Profile, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})



class Workgroup(models.Model):

    class Meta:
        verbose_name = "workgroup"
        verbose_name_plural = "workgroups"

    label = models.CharField(max_length=150,blank=False,null=False)
    users = models.ManyToManyField(Profile)
    workplace = models.ForeignKey(Workplace, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})

class Project(models.Model):

    class Meta:
        verbose_name = "project"
        verbose_name_plural = "projects"

    label = models.CharField(max_length=150,blank=False,null=False)
    workgroups = models.ManyToManyField(Workgroup)
    workplace = models.ForeignKey(Workplace, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})

class App(models.Model):

    class Meta:
        verbose_name = "app"
        verbose_name_plural = "apps"

    name = models.CharField(max_length=150,blank=False,null=False)
    description = models.CharField(max_length=150,blank=False,null=False)
    url = models.CharField(max_length=150,blank=False,null=False)
    version = models.CharField(max_length=50,blank=False,null=False)

    installed_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})
