#
# OpenGDToolbox : collaborative web app made for game creators to simplify game
# design work flow and collaboration.
# Copyright (C) 2019 Thibaud FAURIE
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'profile', views.restViews.ProfileView, 'profile')
router.register(r'workplace', views.restViews.WorkplaceView, 'workplace')
router.register(r'workgroup', views.restViews.WorkgroupView, 'workgroup')
router.register(r'project', views.restViews.ProjectView, 'project')

urlpatterns = [
    path('', views.index),
    path('workspace', views.views.workspace),
    path('login', views.views.login),
    path('logout', views.views.logout),
    path('session', views.views.session),

    path('api/', include(router.urls)),

    path('ajax/wp', views.embededViews.workplaceSelector),
    path('ajax/admin', views.embededViews.adminPanel),
    path('ajax/apps', views.embededViews.appSelector),

    path('ajax/wg', views.utilsViews.workgroupRegister),
    path('ajax/wg/del', views.utilsViews.workgroupUnregister),
    path('ajax/pj', views.utilsViews.projectRegister),
    path('ajax/pj/del', views.projectUnregister),

    path('ajax/tabs/save', views.utilsViews.saveTab),
    path('ajax/tabs/del', views.utilsViews.delTab),
    path('ajax/tabs/load', views.utilsViews.loadTabs),
]
