#
# OpenGDToolbox : collaborative web app made for game creators to simplify game
# design work flow and collaboration.
# Copyright (C) 2019 Thibaud FAURIE
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

import logging
import logging.config

import django.middleware.csrf

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.template import Context, Template, loader, RequestContext
from django.contrib import messages
from rest_framework import viewsets
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated
from ..serializers import ProfileSerializer, WorkplaceSerializer, WorkgroupSerializer, ProjectSerializer
from ..models import Profile, Workplace, Workgroup, Project

logger = logging.getLogger(__name__)

# Views

def index(request):
    if request.user.is_authenticated:
        return redirect('/workspace')
    else:
        context = {}
        template = loader.get_template('manager/Index.html')
        return HttpResponse(template.render(context, request))

def login(request):
    if request.user.is_authenticated:
        return redirect('/workspace')
    else:
        context = {}
        if request.method == 'POST':
            if(request.POST['id'] == '' or request.POST['password'] == ''):
                messages.warning(request,'Please enter a username and a password !')
                template = loader.get_template('manager/Login.html')
                return HttpResponse(template.render(context, request))
            else:
                user = authenticate(username=request.POST['id'], password=request.POST['password'])
                if user is not None:
                    auth_login(request, user)
                    remember = request.POST.get('remember', False)
                    if(remember != False):
                        request.session.set_expiry(604800)
                    else:
                        request.session.set_expiry(0)
                    messages.success(request,'Successfuly logged in !')
                    return redirect('/workspace')
                else:
                    messages.error(request,'ID or password incorrect !')
                    template = loader.get_template('manager/Login.html')
                    return HttpResponse(template.render(context, request))
        else:
            template = loader.get_template('manager/Login.html')
            return HttpResponse(template.render(context, request))

@login_required(redirect_field_name="next",login_url='/login')
def logout(request):
    auth_logout(request)
    return redirect('/')

@login_required(redirect_field_name="next",login_url='/login')
def workspace(request):
    context = {}
    template = loader.get_template('manager/Workspace.html')
    return HttpResponse(template.render(context, request))

@login_required(redirect_field_name="next",login_url='/login')
def session(request):
    dump = ''
    for key, value in request.session.items():
        row = '{%s} => {%s}<br/><br/>' % (key, value)
        dump += row
    return HttpResponse(dump,status=200)
