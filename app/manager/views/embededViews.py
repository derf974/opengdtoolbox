#
# OpenGDToolbox : collaborative web app made for game creators to simplify game
# design work flow and collaboration.
# Copyright (C) 2019 Thibaud FAURIE
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

import logging
import logging.config

import django.middleware.csrf
from ..common.decorators import ajax_required

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.template import Context, Template, loader, RequestContext
from rest_framework import viewsets
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated
from ..serializers import ProfileSerializer, WorkplaceSerializer, WorkgroupSerializer, ProjectSerializer
from ..models import Profile, Workplace, Workgroup, Project, App

# Embeded views

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def workplaceSelector(request):
    if 'workplace' not in request.POST:
        queryset = Workplace.objects.filter(
            users__id = request.user.id
        )
        context = {'workplaces':queryset}
        template = loader.get_template('manager/WorkplaceSelector.html')
        return HttpResponse(template.render(context, request))
    else:
        request.session['workplace']=request.POST.get('workplace')
        return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def appSelector(request):
    workplace = request.session.get('workplace', None)
    queryset = App.objects.all()
    context = {'apps':queryset,'workplace':workplace}
    template = loader.get_template('manager/AppSelector.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminPanel(request):
    context = {}
    template = loader.get_template('manager/Admin.html')
    return HttpResponse(template.render(context, request))
