#
# OpenGDToolbox : collaborative web app made for game creators to simplify game
# design work flow and collaboration.
# Copyright (C) 2019 Thibaud FAURIE
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

import logging
import logging.config

import json

import django.middleware.csrf
from ..common.decorators import ajax_required

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.template import Context, Template, loader, RequestContext
from django.contrib import messages
from rest_framework import viewsets
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated
from ..serializers import ProfileSerializer, WorkplaceSerializer, WorkgroupSerializer, ProjectSerializer
from ..models import Profile, Workplace, Workgroup, Project

# Utils views
@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def workgroupRegister(request):
    if request.session.get('workgroup') and request.POST.get('change') is None :
        return JsonResponse({'registered':request.session.get('workgroup_label')},status=200)
    if 'workgroup' in request.POST:
        request.session['workgroup']=request.POST.get('workgroup')
        request.session['workgroup_label']=request.POST.get('workgroup_label')
        return HttpResponse('',status=200)
    else:
        return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def workgroupUnregister(request):
    if request.session.get('workgroup'):
        request.session.modified = True
        del request.session['workgroup']

    return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def projectRegister(request):
    if request.session.get('project') and request.POST.get('change') is None :
        return JsonResponse({'registered':request.session.get('project_label')},status=200)
    if 'project' in request.POST:
        request.session['project']=request.POST.get('project')
        request.session['project_label']=request.POST.get('project_label')
        return HttpResponse('',status=200)
    else:
        return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def projectUnregister(request):
    if request.session.get('project'):
        request.session.modified = True
        del request.session['project']

    return HttpResponse('',status=200)

# @ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def saveTab(request):
    if not request.session.get('tabs'):
        request.session['tabs'] = {}
    request.session.modified = True
    request.session['tabs'][request.POST.get('id')] = {
            'title':request.POST.get('title'),
            'url':request.POST.get('url'),
            'app':request.POST.get('app'),
            'appdata':request.POST.get('appdata') or '',
        }
    return HttpResponse('',status=200)

# @ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def delTab(request):
    request.session.modified = True
    if request.session.get('tabs', None):
        if request.POST.get('id') == '*':
            del request.session['tabs']
        else:
            del request.session['tabs'][request.POST.get('id')]
    return HttpResponse('',status=200)

# @ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def loadTabs(request):
    if request.session.get('tabs', None):
        return JsonResponse(request.session.get('tabs'))
    else:
        return JsonResponse({})
