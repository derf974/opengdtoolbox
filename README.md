# OpenGDToolbox

**OpenSource** application under **GPL-3.0-or-later** designed to provide a complete, easy to use and free tools library for collaborative work on Game Design projects and concepts.

## Technicals details

The project is maintained under Django Frameworks and using Python as back-end main language.

Front-end is maintained with npm modules as gulp-sass, bootstrap and jquery.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Make sure you have docker and docker-compose installed.

    `apt-get install docker docker-compose`

2. Install git for being able to clone the project, make commits and push, etc...

    `apt-get install git`
    
3. Install nodejs and yarn.

    `apt-get install nodejs npm`
    
    `curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list`
    
    `apt-get update | apt-get install yarn`

### Installing

1. First, you need to clone the project and install dependencies with following command lines :

    `git clone https://gitlab.com/moulagofre/opengdtoolbox`
    
    `cd opengdtoolbox`
    
    `yarn install`

2. Then, when cloning is complete, you can run gulp routines to make front-end working.
    
    `yarn run gulp styles`

3. Build & launch docker containers :

    `docker-compose build`

    `docker-compose up -d`

4. Make migrations and create Django superuser :

    `docker-compose exec app python manage.py migrate`

    `docker-compose exec app python manage.py createsuperuser`

### Running the tests

*Will come later*

## Deployment / production server installation

*Will come later*

## Built With

* Django - The web framework used
* npm/yarn - Dependency Management
* Bootstrap - CSS framework
* jquery - JS framework
* Networkx - Schemas and diagrams JS library
* Docker - containers
